---
title: "R Workshop: Day 2"
params:
  run_check: TRUE
output:
  rmdformats::readthedown:
    highlight: pygments
    css: media/style.css
    df_print: paged
    toc_depth: 3
---

```{r include=FALSE}
source("00_Setup_Knitr.R")
```

```{r child = '01_Code_Structure.Rmd'}
```

```{r child = '02_Code_Style.Rmd'}
```

```{r child = '03_Reporting.Rmd'}
```

```{r child = '03_Reporting_Dashboards.Rmd'}
```

```{r child = '90_Appendix_Footnotes.Rmd'}
```

